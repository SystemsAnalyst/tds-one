// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			MovementTick(DeltaSeconds);
			/* логика вынесена в отдельный метод
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
			*/
		}
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisForwardX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisRightY);
}

void ATDSCharacter::InputAxisForwardX(float Value) {
	 AxisX = Value;
}

void ATDSCharacter::InputAxisRightY(float Value) {
	AxisY = Value;
}

void ATDSCharacter::MovementTick(float DeltaTime) {
	AddMovementInput(FVector(1.0f,0.0f,0.0f), AxisX);
	AddMovementInput(FVector(0.0f,1.0f,0.0f), AxisY);

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if(MyController) {
		FHitResult TraceHitResult;
		// Разворачиваем перса только в том случае, если курсор поймал объект во вьюпорте.
		// Иначе оставляем положение неизменным
		if(MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, true, TraceHitResult)) {
			//		FRotator Rotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);
			//		float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
			//		SetActorRotation(FRotator(0.0f,Yaw,0.0f));

			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
			
			// 2 варианта
			FRotator Rotator = FRotationMatrix::MakeFromX(TraceHitResult.Location - GetActorLocation()).Rotator();
			Rotator.Roll = 0.0f;
			Rotator.Pitch = 0.0f;
			SetActorRotation(Rotator);
		}
	}
}


void ATDSCharacter::UpdateCharacter() {
	const float NewSpeed = MovementInfo.GetSpeedByState(MovementState);
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 400.0f, FColor::Yellow, *FString::Printf(TEXT("Speed = %.3f"), NewSpeed), true);
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed; 
}

void ATDSCharacter::ChangeMovementState() {

	
	if(SprintEnabled) {
		MovementState = EMovementState::SprintRun_State;
		WalkEnabled = false;
		AimEnabled = false;
	} else if (WalkEnabled && AimEnabled){
		MovementState = EMovementState::AimWalk_State;
	} else if(WalkEnabled) {
		MovementState = EMovementState::Walk_State;
	} else if(AimEnabled) {
		MovementState = EMovementState::Aim_State;
	} else {
		MovementState = EMovementState::Run_State;
	}
	
	UpdateCharacter();
}
