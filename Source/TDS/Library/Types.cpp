// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
//#include "TDS/TDS.h"


float FMovementInfo::GetSpeedByState(const EMovementState NewMovementState) {

	float NewSpeed = 600.0f;
	switch(NewMovementState) {
	case EMovementState::Aim_State:
		NewSpeed = AimSpeedNormal;	
		break;
	case EMovementState::AimWalk_State:
		NewSpeed = AimSpeedWalk;	
		break;
	case EMovementState::Walk_State:
		NewSpeed = WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		NewSpeed = RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		NewSpeed = RunSpeedSprint;
		break;
	}
	
	return NewSpeed;
};	
