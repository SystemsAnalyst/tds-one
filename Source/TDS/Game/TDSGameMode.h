// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameMode.h"
#include "GameFramework/GameModeBase.h"
#include "TDSGameMode.generated.h"

UCLASS(minimalapi)
class ATDSGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATDSGameMode();
};



